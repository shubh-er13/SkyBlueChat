package your.packa.namespace;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.UUID;
import android.app.Activity;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore.MediaColumns;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

public class SkybluechatActivity extends Activity 
{
	BluetoothAdapter madapter;
	ImageView iv;
	ArrayList<UUID> mUuids;
	static LinkedList<OutputStream> outs;
	LinkedList<BluetoothDevice> devs;
	static LinkedList<ConnectedThread> mthreads;
	LinkedList<String> addresses;
	public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    AcceptThread at;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        outs=new LinkedList<OutputStream>();
        addresses=new LinkedList<String>();
        mthreads=new LinkedList<ConnectedThread>();
        mUuids = new ArrayList<UUID>();
        // 7 randomly-generated UUIDs. These must match on both server and client.
        mUuids.add(UUID.fromString("b7746a40-c758-4868-aa19-7ac6b3475dfc"));
        mUuids.add(UUID.fromString("2d64189d-5a2c-4511-a074-77f199fd0834"));
        mUuids.add(UUID.fromString("e442e09a-51f3-4a7b-91cb-f638491d1412"));
        mUuids.add(UUID.fromString("a81d6504-4536-49ee-a475-7d96d09439e4"));
        mUuids.add(UUID.fromString("aa91eab1-d8ad-448e-abdb-95ebba4a9b55"));
        mUuids.add(UUID.fromString("4d34da73-d0a4-4f40-ac38-917e0a9dee97"));
        mUuids.add(UUID.fromString("5e14d4df-9c8a-4db7-81e4-c937564c86e0"));
        iv=(ImageView)findViewById(R.id.imageView2);
        madapter=BluetoothAdapter.getDefaultAdapter();
        if(madapter==null)
        {
        	Toast t=Toast.makeText(this, "no support for bluetooth", Toast.LENGTH_LONG);
        	t.show();
        	iv.setClickable(false);
        }
        else if(!madapter.isEnabled())
        {
        	Intent intn=new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        	startActivityForResult(intn, 1);
        	iv.setClickable(true);
        }
        else
        {
        	iv.setClickable(true);
        }
    }
    public void startBluetooth(View arg)
    {
    	setContentView(R.layout.chat);
    }
    public void webbrowser(View arg)
	{
		Uri uriUrl = Uri.parse("http://www.google.com/");  
		Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
		startActivity(launchBrowser);
	}
    public void startDiscoverable(View arg)
    {
    	Intent intn=new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
    	intn.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
    	startActivity(intn);
    }
    Dialog hdlg;
    public void Help(View arg)
    {
    	hdlg=new Dialog(this);
		hdlg.setContentView(R.layout.help);
		Button b=(Button)hdlg.findViewById(R.id.button1);
		hdlg.setTitle("Help");
		b.setOnClickListener(new View.OnClickListener()
		{
			
			public void onClick(View v) {
				hdlg.dismiss();
			}
		});
		hdlg.show();
    }
    public void setting(View arg0)
    {
    	Intent intentBluetooth = new Intent();
        intentBluetooth.setAction(android.provider.Settings.ACTION_BLUETOOTH_SETTINGS);
        startActivity(intentBluetooth); 
    }
    Dialog fdlg;
    ArrayAdapter<String> farr;
    ArrayList<String> fal;
    ListView flv;
    public void devicelist(View arg)
    {
    	for(int i=0;i<mthreads.size();i++)
    	{
    		ConnectedThread ct=(ConnectedThread)mthreads.get(i);
    		ct.write((madapter.getAddress()+"#"+"FileRequest").getBytes());
    	}
    	fdlg=new Dialog(this);
    	fdlg.setTitle("Shared Files");
    	fdlg.setContentView(R.layout.sharedfiles);
    	flv=(ListView)fdlg.findViewById(R.id.listView1);
    	flv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
    		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
    				long arg3) {
    			for(int i=0;i<mthreads.size();i++)
    	    	{
    	    		ConnectedThread ct=(ConnectedThread)mthreads.get(i);
    	    		ct.write((madapter.getAddress()+"#"+"FileDownload#"+flv.getItemAtPosition(arg2)).getBytes());
    	    		fdlg.dismiss();
    	    	}
    		}
		});
    	fal=new ArrayList<String>();
    	farr=new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,fal);
    	flv.setAdapter(farr);
    	fdlg.show();
    }
    Button b1,b2;
    EditText t;
    public void shareViaBluetooth(View arg)
    {
    	dlg=new Dialog(this);
		dlg.setContentView(R.layout.browse);
		b1=(Button)dlg.findViewById(R.id.button1);
		b2=(Button)dlg.findViewById(R.id.button2);
		t=(EditText)dlg.findViewById(R.id.editText1);
		b2.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				dlg.dismiss();
			}
		});
		b1.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent intent=new Intent();
				intent.setType("*/*");
				intent.setAction(Intent.ACTION_GET_CONTENT);
				startActivityForResult(Intent.createChooser(intent, "Select File"),PICKIMAGE);
			}
		});
		dlg.setCancelable(true);
		dlg.setTitle("Upload Something");
		dlg.show();
    }
    final static int PICKIMAGE=1;
	String filename;
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(resultCode==Activity.RESULT_OK)
		{
			if(requestCode==PICKIMAGE)
			{
				try{
				Uri uri=data.getData();
				String selpath=getPath(uri);
				t.setText(selpath);
				ContentValues values = new ContentValues();
				values.put(BluetoothShare.URI, uri+"");
				values.put(BluetoothShare.DESTINATION, connectedaddress);
				values.put(BluetoothShare.DIRECTION, BluetoothShare.DIRECTION_OUTBOUND);
				Long ts = System.currentTimeMillis();
				values.put(BluetoothShare.TIMESTAMP, ts);
				getContentResolver().insert(BluetoothShare.CONTENT_URI, values);
				}catch(Exception e)
				{
					System.out.println("Exception is "+e);
				}
			}
		}
	};
	public String getPath(Uri uri)
	{
		String[] projection={MediaColumns.DATA};
		Cursor cursor=managedQuery(uri, projection, null, null, null);
		int index=cursor.getColumnIndexOrThrow(MediaColumns.DATA);
		cursor.moveToFirst();
		return cursor.getString(index);
	}
    ListView devices;
    ArrayAdapter<String> arr;
    ArrayList<String> al;
    Dialog dlg;
    public void startClient(View arg)
    {
    	IntentFilter filter1=new IntentFilter(
    			BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
    	registerReceiver(finishreceiver,filter1);
    	IntentFilter filter2=new IntentFilter(
    			BluetoothDevice.ACTION_FOUND);
    	registerReceiver(foundreceiver,filter2);
    	discover.start();
    	dlg=new Dialog(this);
    	dlg.setCancelable(true);
    	dlg.setTitle("Device List");
    	dlg.setContentView(R.layout.devices);
    	devices=(ListView)dlg.findViewById(R.id.listView1);
    	devices.setOnItemClickListener(new AdapterView.OnItemClickListener() {
    		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
    				long arg3) {
    			madapter.cancelDiscovery();
    			BluetoothDevice dev=(BluetoothDevice)devs.get(arg2);
    			dlg.dismiss();
    			for(int i=0;i<mUuids.size();i++)
    			{
    				ConnectThread ct=new ConnectThread(dev,mUuids.get(i));
    				ct.start();
    			}
    		}
    	});
    	al=new ArrayList<String>();
    	arr=new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,al);
    	devices.setAdapter(arr);
    	devs=new LinkedList<BluetoothDevice>();
    	dlg.show();
    }
    private Thread discover=new Thread(new Runnable() {
		
		public void run() {
			if(madapter!=null)
				madapter.startDiscovery();
		}
	});
    private BroadcastReceiver foundreceiver=new BroadcastReceiver() {
		
		@Override
		public void onReceive(Context arg0, Intent arg1) 
		{
			BluetoothDevice device=arg1.getParcelableExtra(
					BluetoothDevice.EXTRA_DEVICE);
			if(device!=null&&!device.getName().equals(""))
			{
				devs.add(device);
				al.add(device.getName());
				arr.notifyDataSetChanged();
			}
		}
	};
	private BroadcastReceiver finishreceiver=new BroadcastReceiver() {
		
		@Override
		public void onReceive(Context arg0, Intent arg1) {
			unregisterReceiver(foundreceiver);
			unregisterReceiver(this);
		}
	};
    @Override
    public boolean onCreateOptionsMenu(Menu menu) 
    {
    	menu.add("Start as Server");
    	menu.add("Exit");
    	return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) 
    {
    	String title=item.getTitle().toString();
    	if(title.equals("Start as Server"))
    	{
    		Toast.makeText(this, "Server Listening...", Toast.LENGTH_LONG).show();
    		at=new AcceptThread();
    		at.start();
    	}
    	else if(title.equals("Exit"))
    	{
    		madapter.cancelDiscovery();
    		finish();
    	}
    	return true;
    }
    public class AcceptThread extends Thread
    {
    	public AcceptThread() {
			
		}
    	public void run()
    	{
    		while(true)
    		{
	    		BluetoothSocket client;
	    		for(int i=0;i<mUuids.size();i++)
	    		{
	    			try{
	    				
	    			BluetoothServerSocket server=madapter.listenUsingRfcommWithServiceRecord("BNEP", mUuids.get(i));
	    			client=server.accept();
	    			if(client!=null)
	    			{
	    				manageConnectedSocket(client,client.getRemoteDevice());
	    			}
	    			}catch(Exception e)
	    			{
	    				System.out.println(e);
	    			}
	    		}
    		}
    	}
    }
    int j;
    public void sendMessageToAll(final String message)
	{
		for(j=0;j<outs.size();j++)
    	{
					OutputStream ct=(OutputStream)outs.get(j);
					try{
		    		ct.write(message.getBytes());
					}catch(Exception e)
					{}		
    	}
	}
    Handler h=new Handler();
    String dv;
    String connectedaddress;
    public void manageConnectedSocket(BluetoothSocket client,BluetoothDevice device)
    {
    	dv=device.getName();
    	connectedaddress=device.getAddress();
    	ConnectedThread ct=new ConnectedThread(client);
    	ct.start();
    	mthreads.add(ct);
    	addresses.add(connectedaddress);
    	h.post(new Runnable() {
			
			public void run() {
				Toast t=Toast.makeText(getApplicationContext(), "Connected to "+dv, Toast.LENGTH_LONG);
		    	t.show();
			}
		});
    }
    public void write(byte[] out) { 
    	for (int i = 0; i < mthreads.size(); i++) {
    		try {
                ConnectedThread r;
                synchronized (this) {
                    r = mthreads.get(i);
                }
                r.write(out);
    		} catch (Exception e) {    			
    		}
    	}
    }
    final Handler mhandler=new Handler(){
    	public void handleMessage(Message msg) {
    		switch(msg.what)
    		{
    		case MESSAGE_WRITE:
                byte[] writeBuf = (byte[]) msg.obj;
                String writeMessage = new String(writeBuf);
                msgal.add(writeMessage);
                try{
            	    final MediaPlayer player = MediaPlayer.create(getApplicationContext(),R.raw.ring);
            	    player.setVolume(100, 100);
            	    player.start();
            	} catch (Exception e) {
                	System.out.println(e);
                }
                msgarr.notifyDataSetChanged();
                break;
            case MESSAGE_READ:
                byte[] readBuf = (byte[]) msg.obj;
                String readMessage = new String(readBuf, 0, msg.arg1);
                if (readMessage!=null&&readMessage.length() > 0) {
                	if(readMessage.indexOf("SharedData#")!=-1)
                	{
                		String filenames[]=readMessage.split("#");
                		for(int i=1;i<filenames.length;i++)
                		{
                			fal.add(filenames[i]);
                			farr.notifyDataSetChanged();
                		}
                	}
                	else
                	{
                		if(readMessage.indexOf("FileRequest")==-1&&readMessage.indexOf("FileDownload")==-1)
                		{
		                    msgal.add(readMessage);
		                    try{
		                	    final MediaPlayer player = MediaPlayer.create(getApplicationContext(),R.raw.ring);
		                	    player.setVolume(100, 100);
		                	    player.start();
		                	} catch (Exception e) {
		                    	System.out.println(e);
		                    }
		                    msgarr.notifyDataSetChanged();
                		}
                	}
                }
                if(at!=null)
                {
                	if(readMessage.indexOf("#FileRequest")!=-1)
                	{
                		int index=readMessage.indexOf("#");
                		String devadd=readMessage.substring(0, index);
                		index=addresses.indexOf(devadd);
                		File dir=Environment.getExternalStorageDirectory();
                		File d=new File(dir.getPath()+"/bluetooth");
                		String array[]=d.list();
                		String datatosend="SharedData#";
                		for(String temp:array)
                		{
                			datatosend+=temp+"#";
                		}
                		try{
                		OutputStream pw=(OutputStream)outs.get(index);
                		pw.write(datatosend.getBytes());
                		pw.flush();
                		}catch(Exception e)
                		{}
                	}
                	else if(readMessage.indexOf("#FileDownload")!=-1)
                	{
                		System.out.println(readMessage);
                		int index=readMessage.indexOf("#");
                		String devadd=readMessage.substring(0, index);
                		String filename=readMessage.substring(readMessage.lastIndexOf("#")+1);
                		File dir=Environment.getExternalStorageDirectory();
                		
                		File d=new File(dir.getPath()+"/bluetooth/"+filename);
                		System.out.println(d.getPath());
                		ContentValues values = new ContentValues();
        				values.put(BluetoothShare.URI, "content://org.openintents.filemanager/mimetype/"+d.getPath());
        				values.put(BluetoothShare.DESTINATION, devadd);
        				values.put(BluetoothShare.DIRECTION, BluetoothShare.DIRECTION_OUTBOUND);
        				Long ts = System.currentTimeMillis();
        				values.put(BluetoothShare.TIMESTAMP, ts);
        				getContentResolver().insert(BluetoothShare.CONTENT_URI, values);
                	}
                	else
                	{
                		sendMessageToAll(readMessage);
                	}
                }
                break; 
    		}
    	};
    };
    public void sendMessage(View arg)
    {
    	for(int i=0;i<mthreads.size();i++)
    	{
    		ConnectedThread ct=(ConnectedThread)mthreads.get(i);
    		ct.write((madapter.getName()+": "+msg.getText()).getBytes());
    	}
    	msg.setText("");
    }
    EditText msg;
    ListView list;
    ArrayAdapter<String> msgarr;
    ArrayList<String> msgal;
    public void groupchat(View arg)
    {
    	setContentView(R.layout.groupchat);
    	msg=(EditText)findViewById(R.id.editText1);
    	list=(ListView)findViewById(R.id.listView1);
    	msgal=new ArrayList<String>();
    	msgarr=new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,msgal);
    	list.setAdapter(msgarr);
    }
    private class ConnectedThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        public ConnectedThread(BluetoothSocket socket) {
            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
                if(at!=null)
                {
                	System.out.println("Coming");
                	outs.add(tmpOut);
                }
            } catch (IOException e) {
                System.out.println(e);
            }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        public void run() {
           
            byte[] buffer = new byte[1024];
            int bytes;

            while (true) {
                try {
                    bytes = mmInStream.read(buffer);
                    mhandler.obtainMessage(MESSAGE_READ, bytes, -1, buffer).sendToTarget();
                } catch (IOException e) {
                   
                   System.out.println(e);
                    break;
                }
            }
        }

        public void write(byte[] buffer) {
            try {
                mmOutStream.write(buffer);
                if(at!=null)
                	mhandler.obtainMessage(MESSAGE_WRITE, -1, -1, buffer).sendToTarget();
            } catch (IOException e) {
                System.out.println(e);
            }
        }

        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                System.out.println(e);
            }
        }
    }
    //private final BluetoothDevice mmDevice;
    private class ConnectThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final BluetoothDevice mmDevice;
        private UUID tempUuid;

        public ConnectThread(BluetoothDevice device, UUID uuidToTry) {
            mmDevice = device;
            BluetoothSocket tmp = null;
            tempUuid = uuidToTry;
            try {
                tmp = device.createRfcommSocketToServiceRecord(uuidToTry);        	
            } catch (IOException e) {
                System.out.println(e);
            }
            mmSocket = tmp;
        }

        public void run() {
            setName("ConnectThread");
            madapter.cancelDiscovery();
            try {
                mmSocket.connect();
            } catch (IOException e) {
            	if (tempUuid.toString().contentEquals(mUuids.get(6).toString())) {
                    //connectionFailed();
            		System.out.println("Failed....");
            	}
                try {
                    mmSocket.close();
                } catch (IOException e2) {
                    System.out.println(e);
                }
                return;
            }

            manageConnectedSocket(mmSocket,mmSocket.getRemoteDevice());
        }

        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                System.out.println(e);
            }
        }
    }
}